FROM sonersivri/java:alpine

MAINTAINER soner sivri <sonersivri@gmail.com>

# Setup useful environment variables
ENV JIRA_HOME     /var/atlassian/application-data/jira
ENV JIRA_INSTALL  /opt/atlassian/jira
ENV JIRA_VERSION  7.2.6

LABEL Description="This image is used to start Atlassian Jira" Vendor="Atlassian" Version="${JIRA_VERSION}"

ENV JIRA_DOWNLOAD_URL https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-${JIRA_VERSION}.tar.gz

ENV RUN_USER            daemon
ENV RUN_GROUP           daemon

RUN set -x \
    && mkdir -p                           "${JIRA_HOME}" \
    && chmod -R 700                       "${JIRA_HOME}" \
    && chown ${RUN_USER}:${RUN_GROUP}     "${JIRA_HOME}" \
    && mkdir -p                           "${JIRA_INSTALL}/conf" \
        && rm -rf                             "${JIRA_INSTALL}/lib/postgres*" \
        && wget -q                             https://jdbc.postgresql.org/download/postgresql-9.4.1212.jar -P "${JIRA_INSTALL}/lib" \
        && curl -Ls                           "${JIRA_DOWNLOAD_URL}" | tar -xz --directory "${JIRA_INSTALL}" --strip-components=1 --no-same-owner \
    && chmod -R 700                       "${JIRA_INSTALL}" \
    && chown -R ${RUN_USER}:${RUN_GROUP}  "${JIRA_INSTALL}" \
    && echo -e                            "\njira.home=${JIRA_HOME}" >> "${JIRA_INSTALL}/atlassian-jira/WEB-INF/classes/jira-application.properties" \
    && xmlstarlet                         ed --inplace \
        --delete                          "Server/@debug" \
        --delete                          "Server/Service/Connector/@debug" \
        --delete                          "Server/Service/Connector/@useURIValidationHack" \
        --delete                          "Server/Service/Connector/@minProcessors" \
        --delete                          "Server/Service/Connector/@maxProcessors" \
        --delete                          "Server/Service/Engine/@debug" \
        --delete                          "Server/Service/Engine/Host/@debug" \
        --delete                          "Server/Service/Engine/Host/Context/@debug" \
                                          "${JIRA_INSTALL}/conf/server.xml" \
    && touch -d "@0"                      "${JIRA_INSTALL}/conf/server.xml"

# Use the default unprivileged account. This could be considered bad practice
# on systems where multiple processes end up being executed by 'daemon' but
# here we only ever run one process anyway.

USER ${RUN_USER}:${RUN_GROUP}

# Expose default HTTP connector port.
EXPOSE 8080

# Set volume mount points for installation and home directory. Changes to the
# home directory needs to be persisted as well as parts of the installation
# directory due to eg. logs.
VOLUME ["${JIRA_INSTALL}", "${JIRA_HOME}"]

# Set the default working directory as the JIRA installation directory.
WORKDIR ${JIRA_INSTALL}

# Run Atlassian JIRA as a foreground process by default.
CMD ["./bin/catalina.sh", "run"]
